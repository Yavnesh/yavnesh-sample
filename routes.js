const express = require('express');
const LoginWithTwitter = require('login-with-twitter')

const tw = new LoginWithTwitter({
    consumerKey: 'gy7geqhfoIoiF669iaVZLjGTO',
    consumerSecret: '7SK1aV0EC7zEULgRqPNRwQAieYGqgNLdSdCV5Rq08z6Z1qMM5Z',
    callbackUrl: 'http://localhost:4000/twitter/callback'
});
// get router instance of express
const router = express.Router();


router.get('/hello', function (req, res) {
    res.send("hello");
});

router.get('/twitter', (req, res) => {

    tw.login((err, tokenSecret, url) => {
        if (err) {
            // Handle the error your way
        }

        // Save the OAuth token secret for use in your /twitter/callback route
        console.log(req.session);
        req.session.tokenSecret = tokenSecret;


        // Redirect to the /twitter/callback route, with the OAuth responses as query params
        res.redirect(url)

    })
});

router.get('/twitter/callback', (req, res) => {
    tw.callback({
        oauth_token: req.query.oauth_token,
        oauth_verifier: req.query.oauth_verifier
    }, req.session.tokenSecret, (err, user) => {
        if (err) {
            // Handle the error your way
        }

        // Delete the tokenSecret securely
        delete req.session.tokenSecret;

        // The user object contains 4 key/value pairs, which
        // you should store and use as you need, e.g. with your
        // own calls to Twitter's API, or a Twitter API module
        // like `twitter` or `twit`.
        // user = {
        //   userId,
        //   userName,
        //   userToken,
        //   userTokenSecret
        // }
        req.session.user = user;

        // Redirect to whatever route that can handle your new Twitter login user details!
        res.redirect('http://localhost:3000')
    });
});

module.exports = router;
